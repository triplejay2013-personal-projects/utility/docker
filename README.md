# docker

Custom docker images intended for gitlab-ci pipelines

# Build & Publish to Gitlab Container Registry

```bash
cd IMAGE_DIR
docker login -u USER -p PASSWORD
docker build -t registry.gitlab.com/triplejay2013-personal-projects/utility/docker/IMAGE_NAME:VERSION .
docker push registry.gitlab.com/triplejay2013-personal-projects/utility/docker/IMAGE_NAME:VERSION
```

where:

- VERSION: latest or semver
- USER: Gitlab username
- PASSWORD: Personal Access Token or gitlab password
- IMAGE_NAME: Under what name to publish this docker image to the registry

# Acknowledgments
